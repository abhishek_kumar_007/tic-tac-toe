import React from 'react';
import styles from './App.module.css';
import GameBoard from "./Containers/GameBoard/GameBoard";

function App() {
    return (
        <div className={styles.App}>
            <header className={styles.AppHeader}>
                Tic Tac Toe
            </header>
            <GameBoard/>
        </div>
    );
}

export default App;
