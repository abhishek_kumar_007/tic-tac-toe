import React, {FC, MouseEvent, useCallback, useEffect, useState} from "react";
import GameBoardView, {GameBoardPlayers, GameBoardSelections} from "../../Components/GameBoard/GameBoard";
import Constants from "../../utils/Constants";

export type GameState = -1 | 0 | 1;

function checkForWinner(
    selections: GameBoardSelections,
    currentPlayer: number,
    boardSize: number,
    rowId: number,
    cellId: number
) {
    // clicked - (X, Y)
    // check for 1 row 1 column - [X, 0...3] and [0..3, Y]
    // if X = 0 and y = size -1
    // is at diagonal = ( Size - X === Y) || (Size + X === Y)

    const currentClickedCell = selections[rowId][cellId];
    if (currentClickedCell === -1) {
        let counter = 0;
        // checking horizontally
        for (let i = 0; i < boardSize; i++) {
            if (selections[rowId][i] === currentPlayer) {
                counter++;
            }
        }

        counter++;

        if (counter === boardSize) {
            return true;
        }

        // checking vertically
        counter = 0;
        for (let j = 0; j < boardSize; j++) {
            if (selections[j][cellId] === currentPlayer) {
                counter++;
            }
        }

        counter++;

        if (counter === boardSize) {
            return true;
        }

        if (((boardSize - 1) - rowId === cellId)) {
            counter = 0;
            for (let j = boardSize - 1, i = 0; j >= 0; j--, i++) {
                if (selections[i][j] === currentPlayer) {
                    counter++;
                }
            }

            counter++;
            if (counter === boardSize) {
                return true;
            }

        } else if (rowId === cellId) {
            counter = 0;
            for (let j = 0, i = 0; j < boardSize; j++, i++) {
                if (selections[i][j] === currentPlayer) {
                    counter++;
                }
            }

            counter++;

            if (counter === boardSize) {
                return true;
            }
        }
    }

    return false;
}

const initialState = {
    currentPlayer: (1 as GameBoardPlayers),
    gameState: (-1 as GameState),
    chanceCount: 0,
    selections: function (boardSize: number) {
        const board: GameBoardSelections = [];
        for (let i = 0; i < boardSize; i++) {
            const row: GameBoardPlayers[] = [];
            for (let j = 0; j < boardSize; j++) {
                row.push((-1 as GameBoardPlayers));
            }
            board.push(row);
        }

        return board;
    }
};

const GameBoard: FC = () => {
    const boardSize = Constants.boardSize;

    const [currentPlayer, setCurrentPlayer] = useState<GameBoardPlayers>(initialState.currentPlayer);
    const [gameState, setGameState] = useState<GameState>(initialState.gameState);
    const [chanceCount, setChanceCount] = useState<number>(initialState.chanceCount);

    const [selections, setSelections] = useState<GameBoardSelections>(initialState.selections(boardSize));


    const memoOnReset = useCallback((event: MouseEvent<HTMLButtonElement>) => {
        setCurrentPlayer(initialState.currentPlayer);
        setGameState(initialState.gameState);
        setChanceCount(initialState.chanceCount);
        setSelections(initialState.selections(boardSize));
    }, [])

    const memoOnGamePlayed = useCallback(
        (event: MouseEvent<HTMLTableCellElement>, rowId: number, cellId: number) => {
            if (gameState === -1) {
                const player = currentPlayer === 1 ? 0 : 1;

                setChanceCount((prevCount) => {
                    return prevCount + 1;
                });

                setCurrentPlayer(player);

                setSelections((prevState) => {
                    const prevStateCopy = [...prevState];
                    prevStateCopy[rowId][cellId] = player;
                    return prevStateCopy;
                });

                const isWon = checkForWinner(
                    selections,
                    player,
                    boardSize,
                    rowId,
                    cellId
                );

                if (isWon) {
                    setGameState(1);
                }
            }

        }, [selections, boardSize, setCurrentPlayer, currentPlayer,
            setChanceCount, setGameState, gameState]
    );

    useEffect(() => {
        if (gameState !== 1 && chanceCount === (boardSize * boardSize)) {
            setGameState(0)
        }
    }, [boardSize, gameState, chanceCount, setGameState])

    return (
        <React.Fragment>
            <GameBoardView
                gameBoardSize={boardSize}
                selections={selections}
                gameState={gameState}
                currentPlayer={currentPlayer}
                onGamePlayed={memoOnGamePlayed}
                onReset={memoOnReset}
            />
        </React.Fragment>
    )
}

export default GameBoard;