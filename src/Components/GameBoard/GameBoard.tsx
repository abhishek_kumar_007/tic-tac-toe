import React, {FC, MouseEvent} from "react";
import GameBoardRow from "./GameBoardRow/GameBoardRow";
import GameBoardCell, {OnCellClickedFn} from "./GameBoardCell/GameBoardCell";
import styles from "./gameBoard.module.css";

export type OnGamePlayedFn = OnCellClickedFn;
export type GameBoardPlayers = 0 | 1 | -1;
export type GameBoardSelections = Array<Array<GameBoardPlayers>>;
export type onResetFn = (event: MouseEvent<HTMLButtonElement>) => void;

export interface Props {
    gameBoardSize: number,
    onGamePlayed: OnGamePlayedFn,
    selections: GameBoardSelections,
    gameState: -1 | 0 | 1,
    currentPlayer: GameBoardPlayers,
    onReset: onResetFn
}

function getGameBoardRow({gameBoardSize, onGamePlayed, selections}: Props) {
    const board = [];
    for (let i = 0; i < gameBoardSize; i++) {
        const row = [];
        for (let j = 0; j < gameBoardSize; j++) {
            row.push(
                <GameBoardCell
                    key={`${i}-${j}`}
                    rowId={i}
                    cellId={j}
                    isCellPlayed={selections[i][j] !== -1}
                    cellPlayedBy={selections[i][j]}
                    onCellClicked={onGamePlayed}
                />
            );
        }
        board.push(
            <GameBoardRow key={`${i}`}>{row}</GameBoardRow>
        );
    }

    return board;
}

const GameBoard: FC<Props> = (props) => {

    const {gameState, currentPlayer, onReset} = props;
    return (
        <section>
            <div className={styles.gameMessage}>
                <div>
                    {gameState === 1 ? (
                        `Player ${currentPlayer === 0 ? 1 : 2} Won!`
                    ) : ""}
                    {gameState === 0 ? (
                        `Match Draw`
                    ) : ""}
                </div>
            </div>
            <table className={styles.gameBoard}>
                <tbody>
                {getGameBoardRow(props)}
                </tbody>
            </table>

            <div className={styles.resetButtonContainer}>
                <button onClick={onReset}>Restart</button>
            </div>
        </section>
    )
}

export default GameBoard;