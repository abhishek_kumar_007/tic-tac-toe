import React, {ElementType, FC} from "react";

export interface Props {
    node?: ElementType
}

const GameBoardRow: FC<Props> = (props) => {
    const {children, node: Component = "tr"} = props;
    return (
        <Component>
            {children}
        </Component>
    )
};

GameBoardRow.defaultProps = {
    node: "tr"
};

export default GameBoardRow;