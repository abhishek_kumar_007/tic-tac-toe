import classNames from "classnames";
import React, {MouseEvent, FC, useCallback} from "react";
import styles from "./gameBoardCell.module.css";

export type OnCellClickedFn = (event: MouseEvent<HTMLTableCellElement>, rowId: number, cellId: number) => void;
export type Player = 0 | 1 | -1;

export interface Props {
    node?: React.ReactElement,
    onCellClicked: OnCellClickedFn,
    rowId: number,
    cellId: number,
    isCellPlayed?: boolean,
    cellPlayedBy: Player
}

function getPlayerId(player: Player) {
    if (player === -1) {
        return "";
    } else if (player === 1) {
        return "0";
    }
    return "X";
}

const GameBoardCell: FC<Props> = (props) => {
    const {
        node: Component,
        onCellClicked,
        cellPlayedBy,
        isCellPlayed,
        rowId,
        cellId
    } = props;

    const memoOnCellClicked = useCallback(
        (event: MouseEvent<HTMLTableCellElement>) => {
            if (!isCellPlayed) {
                onCellClicked(event, rowId, cellId);
            }
        }, [isCellPlayed, onCellClicked, cellId, rowId]
    );

    const gameBoardCellClassName = classNames(styles.gameBoardCell, {
        [styles.gameBoardPlayer1Cell]: cellPlayedBy === 0,
        [styles.gameBoardPlayer2Cell]: cellPlayedBy === 1
    })

    return (
        <td onClick={memoOnCellClicked} className={gameBoardCellClassName}>
            {getPlayerId(cellPlayedBy)}
        </td>
    )
};

GameBoardCell.defaultProps = {
    isCellPlayed: false
}

export default GameBoardCell;